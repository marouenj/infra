sudo apt-get -y update

# unzip
sudo apt-get -y install unzip

# java
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get -y update

echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections

sudo apt-get -y install oracle-java8-installer
sudo apt-get -y install oracle-java8-set-default

# ntp
sudo apt-get -y install ntp

# iptables
sudo ufw disable

# ambari
sudo wget -nv http://public-repo-1.hortonworks.com/ambari/ubuntu14/2.x/updates/2.2.2.0/ambari.list -O /etc/apt/sources.list.d/ambari.list
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com B9733A7A07513CAD
sudo apt-get -y update
# apt-cache showpkg ambari-server
# apt-cache showpkg ambari-agent
# apt-cache showpkg ambari-metrics-assembly
sudo apt-get -y install ambari-server
sudo ambari-server setup --java-home /usr/lib/jvm/java-8-oracle --silent
sudo ambari-server start

