// variable
variable "access" {}
variable "secret" {}

variable "Name" {
    type = "string"
    default = "HDP"
}

provider "aws" {
    region = "ap-northeast-1"

    access_key = "${var.access}"
    secret_key = "${var.secret}"
}

resource "aws_vpc" "default" {
    cidr_block = "10.0.0.0/16"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_route" "default" {
    route_table_id = "${aws_vpc.default.main_route_table_id}"

    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
}

resource "aws_security_group_rule" "default" {
    security_group_id = "${aws_vpc.default.default_security_group_id}"

    type = "ingress"
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_subnet" "default" {
    vpc_id = "${aws_vpc.default.id}"

    availability_zone = "ap-northeast-1a"
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true"

    tags {
        Name = "${var.Name}"
    }
}

resource "template_file" "key" {
    template = "${file("ap-northeast-1.pem")}"

    vars {}
}

resource "template_file" "server" {
    template = "${file("user_data.yaml")}"

    vars {
        hn = "server"
    }
}

resource "aws_instance" "ambari" {
    instance_type = "t2.micro"
    ami = "ami-9152b8f0"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "10.0.1.10"

    associate_public_ip_address = "true"

    user_data = "${template_file.server.rendered}"

    provisioner "file" {
        source = "./id_rsa"
        destination = "/home/ubuntu/.ssh/id_rsa"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }

    provisioner "remote-exec" {
        inline = [
        "sudo chown 600 /home/ubuntu/.ssh/id_rsa"
        ]
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }
}

resource "template_file" "agent" {
    template = "${file("user_data.yaml")}"

    vars {
        hn = "agent"
    }
}

resource "aws_instance" "agent" {
    depends_on = ["aws_instance.ambari"]

    instance_type = "t2.micro"
    ami = "ami-9152b8f0"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "10.0.1.20"

    associate_public_ip_address = "true"

    user_data = "${template_file.agent.rendered}"

    provisioner "file" {
        source = "./id_rsa.pub"
        destination = "/home/ubuntu/.ssh/id_rsa.pub"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }

    provisioner "remote-exec" {
        inline = [
        "cat /home/ubuntu/.ssh/id_rsa.pub >> /home/ubuntu/.ssh/authorized_keys"
        ]
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }
}
