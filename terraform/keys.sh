export TF_VAR_access=$(awk 'BEGIN {FS = ","}; FNR==2 {print $2};' credentials.csv)
export TF_VAR_secret=$(awk 'BEGIN {FS = ","}; FNR==2 {print $3};' credentials.csv)

export TF_VAR_dnsimple_email=$(awk 'BEGIN {FS = ","}; FNR==2 {print $1};' credentials_dnsimple.csv)
export TF_VAR_dnsimple_token=$(awk 'BEGIN {FS = ","}; FNR==2 {print $2};' credentials_dnsimple.csv)
export TF_VAR_dnsimple_domain=$(awk 'BEGIN {FS = ","}; FNR==2 {print $3};' credentials_dnsimple.csv)
