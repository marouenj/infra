#!/bin/bash

export ACCESS=$(awk 'BEGIN {FS = ","}; FNR==2 {print $2};' credentials.csv)
export SECRET=$(awk 'BEGIN {FS = ","}; FNR==2 {print $3};' credentials.csv)

packer build template.json
