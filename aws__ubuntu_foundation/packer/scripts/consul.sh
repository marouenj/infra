# update package information
sudo apt-get update

# install unzip
sudo apt-get -y install unzip

# install consul
CONSUL=consul_0.6.4_linux_amd64.zip
cd /tmp
wget https://releases.hashicorp.com/consul/0.6.4/${CONSUL}
unzip ${CONSUL}
rm ${CONSUL}

# set up consul
sudo mv consul /usr/local/bin/
sudo mkdir /etc/consul.d
sudo mkdir /var/lib/consul

# set up systemd for consul
sudo mv /tmp/consul.service /lib/systemd/system/consul.service

# Configure consul to start on boot
sudo systemctl enable /lib/systemd/system/consul.service
