# update package information
sudo apt-get update

# install unzip
sudo apt-get -y install unzip

# install nomad
VERSION=0.3.2
NOMAD=nomad_${VERSION}_linux_amd64.zip
cd /tmp
wget https://releases.hashicorp.com/nomad/${VERSION}/${NOMAD}
unzip ${NOMAD}
rm ${NOMAD}

# set up nomad
sudo mv nomad /usr/local/bin/
sudo mkdir /etc/nomad.d
sudo mkdir /var/lib/nomad

# set up systemd for nomad
sudo mv /tmp/nomad.service /lib/systemd/system/nomad.service

# Configure nomad to start on boot
sudo systemctl enable /lib/systemd/system/nomad.service
