export TF_VAR_access=$(awk 'BEGIN {FS = ","}; FNR==2 {print $2};' credentials.csv)
export TF_VAR_secret=$(awk 'BEGIN {FS = ","}; FNR==2 {print $3};' credentials.csv)

