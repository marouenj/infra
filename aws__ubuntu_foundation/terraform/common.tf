variable "access" {}
variable "secret" {}

provider "aws" {
    region = "ap-northeast-1"
    access_key = "${var.access}"
    secret_key = "${var.secret}"
}

resource "template_file" "default" {
    template = "${file("user_data")}"

    vars {}
}

resource "aws_instance" "default" {
    ami = "ami-0e07e56f"
    instance_type = "t2.micro"
    count = "1"
    key_name = "ap-northeast-1"
    user_data = "${template_file.default.rendered}"
}

resource "aws_volume_attachment" "consul" {
    volume_id = "vol-c9d06e37"
    device_name = "/dev/sdi"
    instance_id = "${aws_instance.default.id}"
}

resource "aws_volume_attachment" "nomad" {
    volume_id = "vol-ddd06e23"
    device_name = "/dev/sdj"
    instance_id = "${aws_instance.default.id}"
}
