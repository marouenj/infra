#!/bin/bash

rm nginx.crt  nginx.key

cat << 'EOF' | openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./nginx.key -out ./nginx.crt
JP
Kanagawa
Yokohama
Kartago
Lamappa
Marouen Jilani
marouen.jilani@gmail.com
EOF

openssl x509 -in ./nginx.crt -text
