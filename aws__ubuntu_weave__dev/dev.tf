// variable
variable "access" {}
variable "secret" {}

variable "dnsimple_email" {}
variable "dnsimple_token" {}
variable "dnsimple_domain" {}

variable "ami" {
    type = "string"
    default = "ami-56649d37"
}

variable "Name" {
    type = "string"
    default = "Development Environment for ..."
}

provider "aws" {
    region = "ap-northeast-1"

    access_key = "${var.access}"
    secret_key = "${var.secret}"
}

resource "aws_vpc" "default" {
    cidr_block = "20.0.0.0/16"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_route" "default" {
    route_table_id = "${aws_vpc.default.main_route_table_id}"

    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
}

resource "aws_security_group_rule" "ssh" {
    security_group_id = "${aws_vpc.default.default_security_group_id}"

    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "http" {
    security_group_id = "${aws_vpc.default.default_security_group_id}"

    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https" {
    security_group_id = "${aws_vpc.default.default_security_group_id}"

    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_default_network_acl" "default" {
    default_network_acl_id = "${aws_vpc.default.default_network_acl_id}"

    ingress {
        protocol   = -1
        rule_no    = 100
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 0
        to_port    = 0
    }

    ingress {
        protocol   = "tcp"
        rule_no    = 105
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 22
        to_port    = 22
    }

    ingress {
        protocol   = "tcp"
        rule_no    = 101
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 80
        to_port    = 80
    }

    ingress {
        protocol   = "tcp"
        rule_no    = 103
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 443
        to_port    = 443
    }

    egress {
        protocol   = -1
        rule_no    = 100
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 0
        to_port    = 0
    }
}

resource "aws_subnet" "default" {
    vpc_id = "${aws_vpc.default.id}"

    availability_zone = "ap-northeast-1a"
    cidr_block = "20.0.1.0/24"
    map_public_ip_on_launch = false

    tags {
        Name = "${var.Name}"
    }
}

resource "template_file" "key" {
    template = "${file("ap-northeast-1.pem")}"

    vars {}
}

// first, weave (bootstrap), artifactory
resource "template_file" "first" {
    template = "${file("user_data_1.yaml")}"

    vars {}
}

resource "aws_instance" "first" {
    instance_type = "t2.micro"
    ami = "${var.ami}"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "20.0.1.10"

    associate_public_ip_address = false

    user_data = "${template_file.first.rendered}"
}

resource "aws_volume_attachment" "artifactory" {
    volume_id = "vol-0fec95f1"
    device_name = "/dev/sdi"
    instance_id = "${aws_instance.first.id}"
}

// second, weave (join), teamcity server
resource "template_file" "second" {
    template = "${file("user_data_2.yaml")}"

    vars {
        weave_address = "${aws_instance.first.private_ip}"
    }
}

resource "aws_instance" "second" {
    depends_on = ["aws_instance.first"]

    instance_type = "t2.micro"
    ami = "${var.ami}"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "20.0.1.11"

    associate_public_ip_address = false

    user_data = "${template_file.second.rendered}"
}

resource "aws_volume_attachment" "teamcity" {
    volume_id = "vol-3ef6b3c0"
    device_name = "/dev/sdi"
    instance_id = "${aws_instance.second.id}"
}

// third, weave (join), teamcity agent
resource "template_file" "third" {
    template = "${file("user_data_3.yaml")}"

    vars {
        weave_address = "${aws_instance.first.private_ip}"
    }
}

resource "aws_instance" "third" {
    depends_on = ["aws_instance.first"]

    instance_type = "t2.micro"
    ami = "${var.ami}"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "20.0.1.12"

    associate_public_ip_address = false

    user_data = "${template_file.third.rendered}"
}

// last, weave (join), nginx
resource "template_file" "last" {
    template = "${file("user_data_last.yaml")}"

    vars {
        weave_address = "${aws_instance.first.private_ip}"
        domain = "${var.dnsimple_domain}"
    }
}

resource "aws_instance" "last" {
    depends_on = ["aws_instance.second"]

    instance_type = "t2.micro"
    ami = "${var.ami}"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "20.0.1.13"

    associate_public_ip_address = true

    user_data = "${template_file.last.rendered}"

    provisioner "file" {
        source = "./nginx.crt"
        destination = "/home/ubuntu/nginx.crt"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }

    provisioner "file" {
        source = "./nginx.key"
        destination = "/home/ubuntu/nginx.key"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }

    provisioner "file" {
        source = "./id_rsa"
        destination = "/home/ubuntu/.ssh/id_rsa"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }

    provisioner "remote-exec" {
        inline = [
        "sudo chown 600 /home/ubuntu/.ssh/id_rsa"
        ]
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.key.rendered}"
        }
    }
}

provider "dnsimple" {
    email = "${var.dnsimple_email}"
    token = "${var.dnsimple_token}"
}

resource "dnsimple_record" "default" {
    domain = "${var.dnsimple_domain}"
    name = "*"
    value = "${aws_instance.last.public_ip}"
    type = "A"
    ttl = 3600
}
