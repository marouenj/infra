A dev environment on AWS comprising:

* Docker (leader and slave nodes)
* Consul server (leader node)
* Nomad server (leader node)
* Consul client (slave node)
* Nomad client (slave node)
