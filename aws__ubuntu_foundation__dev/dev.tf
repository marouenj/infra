// variable
variable "access" {}
variable "secret" {}

variable "Name" {
    type = "string"
    default = "Development Environment for ..."
}

// aws
provider "aws" {
    region = "ap-northeast-1"

    access_key = "${var.access}"
    secret_key = "${var.secret}"
}

// network infra
resource "aws_vpc" "default" {
    cidr_block = "10.0.0.0/16"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_route" "default" {
    route_table_id = "${aws_vpc.default.main_route_table_id}"

    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
}

resource "aws_security_group_rule" "default" {
    security_group_id = "${aws_vpc.default.default_security_group_id}"

    type = "ingress"
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_subnet" "default" {
    vpc_id = "${aws_vpc.default.id}"

    availability_zone = "ap-northeast-1a"
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true"

    tags {
        Name = "${var.Name}"
    }
}

// leader node
resource "template_file" "leader" {
    template = "${file("user_data_leader")}"

    vars {}
}

resource "aws_instance" "leader" {
    instance_type = "t2.micro"
    ami = "ami-79b75a18"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "10.0.1.10"

    associate_public_ip_address = "true"

    user_data = "${template_file.leader.rendered}"
}

resource "aws_volume_attachment" "consul" {
    volume_id = "vol-3d8b33c3"
    device_name = "/dev/sdi"
    instance_id = "${aws_instance.leader.id}"
}

resource "aws_volume_attachment" "nomad" {
    volume_id = "vol-3c8b33c2"
    device_name = "/dev/sdj"
    instance_id = "${aws_instance.leader.id}"
}

// slave node
resource "template_file" "slave" {
    template = "${file("user_data_slave")}"

    vars {}
}

resource "aws_instance" "slave" {
    depends_on = ["aws_instance.leader"]

    instance_type = "t2.micro"
    ami = "ami-79b75a18"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "10.0.1.20"

    associate_public_ip_address = "true"

    user_data = "${template_file.slave.rendered}"
}

resource "aws_volume_attachment" "consul_client" {
    volume_id = "vol-c48c343a"
    device_name = "/dev/sdi"
    instance_id = "${aws_instance.slave.id}"
}

resource "aws_volume_attachment" "nomad_client" {
    volume_id = "vol-228b33dc"
    device_name = "/dev/sdj"
    instance_id = "${aws_instance.slave.id}"
}
