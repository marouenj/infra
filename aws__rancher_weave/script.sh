# make user-docker accessible over tcp
sudo ros config merge << "EOF"
rancher:
  docker:
    extra_args:
    - -H
    - tcp://127.0.0.1:2375
EOF

sudo system-docker pull marouenj/wait-for-docker:latest

# don't merge this to below config!
# when the volume is mounted, user-docker isn't up yet i.e. the socket isn't constructed yet
# connect to tcp instead
# 
# sudo ros config merge << "EOF"
# rancher:
#   services:
#     wait-for-docker:
#       volumes:
#       - /var/run/docker.sock:/var/run/docker.sock
# EOF

sudo ros config merge << "EOF"
rancher:
  services:
    wait-for-docker:
      environment:
        DOCKER_HOST: tcp://127.0.0.1:2375
      image: marouenj/wait-for-docker:latest
      labels:
        io.rancher.os.after: docker
        io.rancher.os.detach: "false"
        io.rancher.os.scope: system
      net: host
      privileged: true
EOF

sudo system-docker pull marouenj/weave:latest

sudo ros config merge << "EOF"
rancher:
  services:
    weave:
      image: marouenj/weave:latest
      labels:
        io.rancher.os.after: wait-for-docker
        io.rancher.os.scope: system
      net: host
      privileged: true
      volumes:
      - /var/run/docker.sock:/var/run/docker.sock
EOF
