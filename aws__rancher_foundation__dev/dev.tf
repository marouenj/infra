variable "access" {}
variable "secret" {}

variable "Name" {
    type = "string"
    default = "Development Environment for ..."
}

provider "aws" {
    region = "ap-northeast-1"

    access_key = "${var.access}"
    secret_key = "${var.secret}"
}

resource "aws_vpc" "default" {
    cidr_block = "10.0.0.0/16"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_route" "default" {
    route_table_id = "${aws_vpc.default.main_route_table_id}"

    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
}

resource "aws_security_group_rule" "default" {
    security_group_id = "${aws_vpc.default.default_security_group_id}"

    type = "ingress"
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_subnet" "default" {
    vpc_id = "${aws_vpc.default.id}"

    availability_zone = "ap-northeast-1a"
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true"

    tags {
        Name = "${var.Name}"
    }
}

resource "aws_instance" "leader" {
    instance_type = "t2.micro"
    ami = "ami-3740a356"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "10.0.1.10"

    associate_public_ip_address = "true"

    user_data = "${file("user_data_one")}"
}

resource "aws_volume_attachment" "leader" {
    volume_id = "vol-23dc76dd"
    device_name = "/dev/sdj"
    instance_id = "${aws_instance.leader.id}"
}

resource "template_file" "slave" {
    template = "${file("user_data_client")}"

    vars {
        consul_address = "${aws_instance.leader.private_ip}"
    }
}

resource "aws_instance" "slave" {
    depends_on = ["aws_instance.leader"]

    instance_type = "t2.micro"
    ami = "ami-3740a356"
    count = "1"

    key_name = "ap-northeast-1"

    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = [ "${aws_vpc.default.default_security_group_id}" ]

    private_ip = "10.0.1.20"

    associate_public_ip_address = "true"

    user_data = "${template_file.slave.rendered}"
}

resource "aws_volume_attachment" "slave" {
    volume_id = "vol-127dcaec"
    device_name = "/dev/sdj"
    instance_id = "${aws_instance.slave.id}"
}
