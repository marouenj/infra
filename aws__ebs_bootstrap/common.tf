variable "access" {}
variable "secret" {}

provider "aws" {
    region = "ap-northeast-1"

    access_key = "${var.access}"
    secret_key = "${var.secret}"
}

resource "template_file" "default" {
    template = "${file("ap-northeast-1.pem")}"

    vars {}
}

resource "aws_instance" "default" {
    instance_type = "t2.micro"
    ami = "ami-5d38d93c"
    count = "1"

    key_name = "ap-northeast-1"

    ebs_block_device {
        volume_type = "gp2"
        volume_size = 1
        device_name = "/dev/sdi"
        delete_on_termination = false
    }

    provisioner "remote-exec" {
        inline = [
        "sudo mkfs.ext4 /dev/xvdi",
        ]
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${template_file.default.rendered}"
        }
    }
}
