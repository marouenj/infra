variable "access" {}
variable "secret" {}

provider "aws" {
    region = "ap-northeast-1"
    access_key = "${var.access}"
    secret_key = "${var.secret}"
}

resource "aws_instance" "default" {
    ami = "ami-22749d43"
    instance_type = "t2.micro"
    count = "1"
    key_name = "ap-northeast-1"
}
