# Update package information, ensure that APT works with the https method, and that CA certificates are installed.
sudo apt-get -y update
sudo apt-get -y install apt-transport-https ca-certificates

# Add the new GPG key.
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

# add docker source for apt
sudo echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" >> /tmp/docker.list
sudo rm /etc/apt/sources.list.d/docker.list
sudo mv /tmp/docker.list /etc/apt/sources.list.d/docker.list

# Update the APT package index.
sudo apt-get -y update

# Purge the old repo if it exists.
sudo apt-get -y purge lxc-docker

# Verify that APT is pulling from the right repository.
apt-cache policy docker-engine

# To install the linux-image-extra package for your kernel version:
sudo apt-get -y update
sudo apt-get -y install -y linux-image-extra-$(uname -r)

# Update your APT package index.
sudo apt-get -y update
sudo apt-get -y install docker-engine

# cache docker images
sudo docker pull nginx:1.11.1
sudo docker pull marouenj/artifactory:latest
sudo docker pull marouenj/teamcity-server:latest
sudo docker pull marouenj/teamcity-agent:latest
sudo docker pull weaveworks/plugin:1.6.0
sudo docker pull weaveworks/weave:1.6.0
sudo docker pull weaveworks/weavedb:latest
sudo docker pull weaveworks/weaveexec:1.6.0