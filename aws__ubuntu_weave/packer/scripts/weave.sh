# update package information
sudo apt-get update

# install weave
curl -L git.io/weave -o /tmp/weave
chmod a+x /tmp/weave
sudo mv /tmp/weave /usr/local/bin/weave
