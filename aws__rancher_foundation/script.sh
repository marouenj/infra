# make user-docker accessible over tcp
sudo ros config merge << "EOF"
rancher:
  docker:
    extra_args:
    - -H
    - tcp://127.0.0.1:2375
EOF

# create a dir to share by tmp-volume
mkdir /home/rancher/nomad

# create a docker volume for sharing /tmp dir (between docker and nomad)
sudo ros config merge << "EOF"
rancher:
  services:
    tmp-volume:
      image: rancher/os-state:v0.4.1
      labels:
        io.rancher.os.before: all-volumes
        io.rancher.os.createonly: "true"
        io.rancher.os.scope: system
      log_driver: json-file
      net: none
      privileged: true
      read_only: true
      volumes:
      - /home/rancher/nomad:/tmp
EOF

# mount tmp volume in docker
sudo ros config set rancher.services.docker.volumes_from "[all-volumes, tmp-volume]"

# cache docker images
# wait-for-docker
sudo system-docker pull marouenj/wait-for-docker:latest
# weave
sudo system-docker pull marouenj/weave:latest
# consul
sudo system-docker pull marouenj/consul:latest
# nomad
sudo system-docker pull marouenj/nomad:latest
# weave
docker pull weaveworks/weaveexec:1.5.0
docker pull weaveworks/weavedb:latest
docker pull weaveworks/weave:1.5.0
docker pull weaveworks/plugin:1.5.0
