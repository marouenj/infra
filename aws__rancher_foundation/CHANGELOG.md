# 0.0.0

FEATURES:

* EBS volumes are mounted before the launch of user Docker
* Cache Consul image for a faster boot up
* Schedule Consul to run on user docker on statup
* Cache Nomad image for a faster boot up
* Schedule Nomad to run on user docker on statup

# 0.0.1

FEATURES:
* EBS volumes are mounted to docker then mapped to the file system inside

BUG FIXES:
* Networked volumes are not visible to containers when first mapped on the host then mounted on containers

# 0.0.2

IMPROVEMENTS

* Schedule services on start-up rather than on build

# 0.0.3

FEATURES:

* Cache docker images for weave
* Create a docker volume for sharing /tmp dir between containers (if needed)
